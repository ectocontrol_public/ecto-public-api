# Ecto Public API
Репозиторий для обратной связи по интеграции с Ectostroy 

<a href="https://help.ectostroy.ru/api/api-description" target="_blank">Документация к публичному API</a> 

# Для чего нужен этот репозиторий?
Здесь вы можете задать свои вопросы по интеграции с Ectostroy, создав issue, а также внести свои предложения по расширению API.
